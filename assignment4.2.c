#include<stdio.h>

int getAttendance(float oldPrice, float newPrice, int avgAtt);
float calCost(float pCost, float cCost, int att);
float calIncome(int attendance, float tPrice);
float calProfit(float income, float cost);

int main()
{
	//constants
	float performanceCost = 500;
	float customerCost = 3;

	//ticket prices
	float oldTicketPrice = 15;
	float newTicketPrice = oldTicketPrice;

	//prifits
	float oldProfit;
	float newProfit;

	//attendance
	int avgAttend = 120;

	//other
	float oldIncome;
	float oldCost;
	int oldAtt;


	float cost = calCost(performanceCost, customerCost, avgAttend);

	float income = calIncome(avgAttend, oldTicketPrice);

	oldProfit = calProfit(income, cost);

	newProfit = oldProfit;

	oldAtt = avgAttend;


	while(oldProfit <= newProfit){
	
		newTicketPrice += 1;
		avgAttend = getAttendance(oldTicketPrice, newTicketPrice, oldAtt);

        	cost = calCost(performanceCost, customerCost, avgAttend);

        	income = calIncome(avgAttend, newTicketPrice);

        	newProfit = calProfit(income, cost);

		if(oldProfit > newProfit)
		{
			printf("Average attendance: %d\n", oldAtt);
			printf("Ticket price: %.2f\n", oldTicketPrice);
			printf("Income: %.2f\n", oldIncome);
			printf("Cost: %.2f\n", oldCost);
			printf("Profit: %.2f\n", oldProfit);

			break;
		}

		oldTicketPrice = newTicketPrice;
		oldProfit = newProfit;
		oldIncome = income;
		oldCost = cost;
		oldAtt = avgAttend;
	
	}

	return 0;
	
}




int getAttendance(float oldPrice, float newPrice, int avgAtt)
{
	return avgAtt + (int)( ((oldPrice - newPrice) * 4)); // 4 = (20.0 / 5)
}

float calCost(float pCost, float cCost, int att)
{
	return pCost + (cCost * att);
}


float calIncome(int attendance, float tPrice)
{
	return attendance * tPrice;
}

float calProfit(float income, float cost)
{
	return income - cost;
}

